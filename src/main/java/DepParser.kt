import java.io.File

class DepParser(val rawLines: List<String>) {

    fun parse() {
        val cleanedLines = rawLines.map(this::cleanInfo)
        val extractModules = extractModulesAndCleanLines(cleanedLines)
        val modulesWithDependencies: List<Module> = convertToModules(extractModules)
        val dependencies = modulesWithDependencies.flatMap(Module::dependencies).toSet()
        val duplicatedVersions: Map<String, List<Dependency>> = dependencies.groupBy { it.depId }.filter { it.value.size > 1 }
        printDuplicatedJar(duplicatedVersions, modulesWithDependencies)

    }

    private fun printDuplicatedJar(duplicatedVersions: Map<String, List<Dependency>>, modulesWithDependecies: List<Module>) {
        for ((depId, versions) in duplicatedVersions) {
            println("Duplicated Dependency: $depId")
            versions.forEach { ver ->
                println("\t Version: ${ver.version} in:")
                modulesWithDependecies.filter { it.containsDependency(ver) }.forEach {
                    println("\t\t${it.moduleArtifact.depId}")
                }
            }
        }
    }

    private fun convertToModules(extractedModules: List<List<String>>): List<Module> {
        return extractedModules
                .map { moduleSpec ->
                    val parentModule = dependencyFromLine(moduleSpec[0])
                    val moduleDeps = extractModuleDependencies(moduleSpec)
                    Module(parentModule, moduleDeps)
                }
    }

    private fun extractModuleDependencies(moduleSpec: List<String>): List<Dependency> {
        return moduleSpec.drop(1)
                .map {
                    dependencyFromLine(removeTreeChars(it))
                }
    }

    private fun dependencyFromLine(line: String): Dependency {
        val split = line.split(":")
        return Dependency(split[0], split[1], split[split.size - 2])
    }

    private fun removeTreeChars(rawModuleName: String) = rawModuleName.substringAfter('-')

    private fun extractModulesAndCleanLines(lines: List<String>): List<List<String>> {
        var startBlock = -1
        var stopBlock = -1
        val groups = mutableListOf<List<String>>()
        for (i in lines.indices) {
            if (lines[i].startsWith("com")) {
                startBlock = i;
            }
            if (lines[i].startsWith("---------------------")) {
                stopBlock = i;
            }
            if (startBlock != -1 && startBlock < stopBlock) {
                val moduleLines = lines.subList(startBlock, stopBlock)
                        .filter { it.isNotBlank() && it.split(":").size > 3 }


                groups.add(moduleLines)
                startBlock = -1
                stopBlock = -1
            }
        }

        return groups

    }

    private fun cleanInfo(rawLine: String): String {
        return rawLine.substringAfter("[INFO] ")

    }


}

fun main(args: Array<String>) {
    val readLines = File(args[0]).readLines()
    DepParser(readLines).parse()
}
