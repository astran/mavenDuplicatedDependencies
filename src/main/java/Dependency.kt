data class Dependency(
        val groupId: String,
        val artifactId: String,
        val version: String
) {
    val depId = "$groupId:$artifactId"
    fun isDuplicated(dep: Dependency) = groupId == dep.groupId && artifactId == dep.artifactId && version != dep.version
}