class Module(val moduleArtifact:Dependency, val dependencies: List<Dependency>) {
   fun containsDependency(depId:String) = dependencies.filter { it.depId == depId }.isNotEmpty()
   fun containsDependency(depId:Dependency) = dependencies.filter { it == depId }.isNotEmpty()
}

